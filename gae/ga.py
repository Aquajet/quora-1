# -*- coding: utf-8 -*-
#!/usr/bin/env python

from uuid import uuid4
from datetime import datetime, timedelta
from urllib import urlencode
from google.appengine.api import urlfetch
class GoogleAnalyticsClass():
    def __init__(self, handler, path="/", dt="Latest news"):
        self.handler = handler
        self.path = path
        self.dt = dt

        user_agent = self.handler.request.headers.get("User-Agent")

        try:
            cid = self.handler.request.cookies["cidic"]
        except KeyError:
            cid = str(uuid4())
            self.handler.response.set_cookie(key='cidic', value=cid, expires=datetime.now() + timedelta(days=2*365), path='/')

        self.track_event_to_ga("UA-59199880-17", cid, user_agent)

    def track_event_to_ga(self, tracking_id, client_id, user_agent):
        language = self.handler.request.headers.get('Accept-Language')
        referer = self.handler.request.headers.get('Referer')
        user_ip = self.handler.request.remote_addr

        form_fields = {
            "v": "1",
            "tid": tracking_id,
            "cid": client_id,
            "t": "pageview",
            "dp": self.path,
            "dt": self.dt,
            "dr": referer,
            "uip": user_ip,
            "ul": language[:2] if language and language[:2] != "" else "nn",
        }
        form_data = urlencode(form_fields)

        result = urlfetch.fetch(url="http://www.google-analytics.com/collect",
                                payload=form_data,
                                method=urlfetch.POST,
                                deadline=15,
                                headers={"Content-Type": "application/x-www-form-urlencoded",
                                         "User-Agent": user_agent})
        return result.status_code

