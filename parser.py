#-*- coding: utf-8 -*-
import requests
import json
import string
import urllib
#import sys
import os
import random
import time
import math
import lxml.html
import logging
import re
from single import SingleInstance

logging.basicConfig(format=u'# %(levelname)-8s [%(asctime)s]  %(message)s',
                    level=logging.DEBUG,
                    filename=os.path.join(os.path.dirname(
                        os.path.abspath(__file__)), 'quora.log'))
logging.getLogger().addHandler(logging.StreamHandler())


class Quora:
    """
        -*- Quora parser -*-
        © Copyright by Fil Andrey (python36@yandex.ru, python36@ex.ua)
    """
    def __init__(self, q, g):
        self.group = g.encode('ascii')
        self.random_bits = map(lambda x: bin(x)[2:].rjust(4, '0'), range(8, 16))
        self.rndm_bits = map(lambda x: bin(x)[2:].rjust(4, '0'), range(32, 64))
        self.session = requests.Session()
        self.server_url = 'http://selanidb.appspot.com/'
        #self.server_url = 'http://127.0.0.1:8080/'
        self.url = 'http://www.quora.com'\
                   '/search?force=1&q=%s&type=' % urllib.quote(q)
        self.headers = {'Accept': ('text/html,application/xhtml+xml,'
                                   'application/xml;q=0.9,image/webp,*/*;'
                                   'q=0.8'),
                        'Accept-Encoding': 'gzip, deflate, sdch',
                        'Accept-Language': 'en-US,en;q=0.8',
                        'Cache-Control': 'max-age=0',
                        'Connection': 'keep-alive',
                        'Cookie': ('m-b="5UKmqFM_NuYi4c1zDInSXA\075\075"; '
                                   'm-s="q_XjKluCQDctHBeYTAwcOQ\075\075"; '
                                   'm-css_v=731a602d47615f3318; '
                                   'm-depd=b7f0ffabd45a2788; '
                                   'm-f=dZ8Ek3LTQdnbQ6v_JF0oHYdzJ4Kxm4eQ_z5U; '
                                   '_gat=1; m-tz=-180;'
                                   '_ga=GA1.2.1800254630.1438190612'),
                        'Host': 'www.quora.com',
                        'User-Agent': ('Mozilla/5.0 '
                                       '(iPad;U;CPU OS 5_1_1 like Mac OS X; '
                                       'zh-cn)AppleWebKit/534.46.0'
                                       '(KHTML, like Gecko)'
                                       'CriOS/19.0.1084.60 Mobile/9B206 '
                                       'Safari/7534.48.3')}

    def start_parse(self, request_type):
        self.url = self.url[:self.url.rindex('type=') + 5] + request_type
        while True:
            page = self.get_page(self.url, self.headers)
            links = set(self.find_links(page))
            for questions_page in self.more_questions(page):
                if questions_page is False:
                    break
                links |= set(self.find_links(questions_page))
            else:
                break
        return links

    def parse(self):
        links = self.start_parse('answer') | self.start_parse('question')
        requests.post(url=self.server_url + 'initial',
                      data={'group': self.group,
                            'token': self.gen_token_initial(self.group)})
        for link in links:
            page = self.get_page('http://www.quora.com' + link)
            try:
                answers = self.f_split(lxml.html.fromstring(page).find_class(
                    'question_text_wrapper')[0].text_content())
            except lxml.etree.XMLSyntaxError:
                logging.warning('lxml.etree.XMLSyntaxError. Skip %s' % link)
                continue
            answers += self.parse_answer(page)
            for answer in self.more_answers(page):
                answers += self.parse_answer(answer)
            answers += self.parse_answer(self.get_page(
                'http://www.quora.com%s/mobile_collapsed' % link))
            self.post_to_server(answers, self.group)
        logging.info('Job done. %d questions parsed' % len(links))

    def post_to_server(self, text, group):
        logging.debug(text)
        requests.post(url=self.server_url,
                      data={'text': json.dumps(text),
                            'group': group,
                            'token': self.gen_token(sum(map(len, text)),
                                                    self.random_bits)})

    def parse_answer(self, text):
        answers, answers_list = lxml.html.fromstring(text), []
        answers_remove = answers.xpath("//div[contains(@class,'AnswerFooter')]")
        answer_text = answers.xpath("//div[contains(@id,'original_answer')]")
        for index, answer in enumerate(answer_text):
            text_answer = answer.text_content()
            _tmp = self.f_split(
                text_answer[:text_answer.rfind(answers_remove[index].text)])
            if not _tmp:
                continue
            answers_list += _tmp
        return answers_list

    def get_page(self, url, headers=None, data=None):
        page = self.session.get(url=url,
                                data=data,
                                headers=headers if headers else self.headers)
        logging.info(" ~ Get page %s" % url)
        return page.text.encode('utf-8')

    def gen_token(self, text, random_bits):
        return hex(int(random.choice(random_bits) +
                   bin(text)[2:] + random.choice(random_bits), 2))[2:]

    def gen_token_initial(self, text):
        return self.gen_token(sum(map(ord, text)), self.rndm_bits)

    def find_links(self, text, start_text='question_link" href="',
                   end_text='"', func=string.find):
        question_links, start_index = [], 0
        while True:
            start_index = func(text, start_text, start_index)
            if not start_index + 1:
                break
            start_index += len(start_text)
            end_index = string.find(text, end_text, start_index)
            question_links.append(text[start_index: end_index])
        logging.info(" + Add question links - %s" % len(question_links))
        return question_links

    def f_index(self, text, begin_text, end_text='",',
                func=string.index, start_index=0):
        begin_index = func(text, begin_text, start_index) + len(begin_text)
        end_index = string.find(text, end_text, begin_index)
        return text[begin_index: end_index]

    def f_rindex(self, text, begin_text, end_text='",', start_index=0):
        return self.f_index(text, begin_text, end_text, string.rindex,
                            start_index)

    def f_find_hashes(self, text, func=f_index):
        return map(lambda x: x[1:],
                   func(text, '"hashes": [', '"],').split('", '))

    def f_rfind_hashes(self, text):
        return self.f_find_hashes(text, self.f_rindex)

    f_start_upper = lambda self, s: string.upper(s[0]) + s[1:]

    def f_split(self, text, sep='.!?', link_end=string.ascii_letters+'+?._%/'):
        strings, _tmp = [], ''
        for char in text + '.':
            if char in sep:
                _tmp = re.sub(r"\s?https?:\/\/\S+", "", _tmp)
                if _tmp and len(_tmp.split(' ', 3)) > 3:
                    strings.append(self.f_start_upper(_tmp.lstrip() + char))
                _tmp = ''
            elif _tmp or char.isalpha():
                _tmp += char
        return strings

    def base36encode(self, number, base36='',
                     alphabet='0123456789'+string.ascii_lowercase):
        while number:
            number, i = divmod(number, 36)
            base36 = alphabet[int(i)] + base36
        return base36

    def f_form_date(self, json_kwargs=None, referring_controller='search',
                    referring_action='index', js_init=None,
                    vcon_method='mobile_get_next_page2',
                    parent_cid='undefined'):
        if json_kwargs is None:
            json_kwargs = {"cid": self.cid,
                           "filter_hashes": self.filter_hashes,
                           "extra_data": {}}
        json_form = json.dumps({"args": [],
                                "kwargs": json_kwargs})
        if js_init is None:
            js_init = {'loading_text': 'Loading...',
                       'serialized_component': self.s_cmp,
                       'has_more': True,
                       'hashes': self.filter_hashes,
                       'extra_data': {},
                       'auto_paged': True,
                       'error_text': ('Connection Error.Tap to retry.'),
                       'not_auto_paged': False}
        self.data = {'json': json_form,
                     'formkey': self.formkey,
                     'window_id': self.window_id,
                     'parent_cid': parent_cid,
                     'referring_controller': referring_controller,
                     'referring_action': referring_action,
                     'js_init': json.dumps(js_init),
                     '__vcon_json': '["hmac","%s"]' % self.vcon_json,
                     '__vcon_method': vcon_method,
                     '__mobile_page': True}

    def more_answers(self, text):
        try:
            _tmp = text.rindex('.QuestionPageA2APreview') + 24
            qid = self.f_index(text, ',{"qid": ', ',', start_index=_tmp)
            s_cmp = self.f_index(text, 'serialized_component": "',
                                 start_index=_tmp)
            num_faces = self.f_index(text, 'num_faces": ', '}',
                                     start_index=_tmp)
            js_init = {"qid": int(qid),
                       "serialized_component": s_cmp,
                       "num_faces": int(num_faces)}
            parent_cid = self.f_index(text, '"', start_index=_tmp)
            self.window_id = self.f_index(text, 'windowId: "')
            self.vcon_json = self.f_rindex(text, '%s": "hmac:' % parent_cid)
            self.f_form_date(json_kwargs={}, referring_controller='question',
                             referring_action='q', js_init=js_init,
                             vcon_method='facepile', parent_cid=parent_cid)

            response = self.session.post(url=self.server_call_url,
                                         data=self.data,
                                         headers=self.headers)
            logging.info(" # Start get more answers")

            self.s_cmp = self.f_index(text, ('user_has_answer"], '
                                             '"serialized_component": "'),
                                      start_index=text.rindex(('Loading...", '
                                                               '"broadcast_relo'
                                                               'ad_data": ["')))
            self.vcon_json = self.f_rindex(text, '"{}": "hmac:'.format(
                self.f_rindex(text, ('new(require("mobile_app2/view/paged_list"'
                                     ').PagedList)("'))))
            self.window_id = self.f_index(text, 'windowId: "')
            self.cid = self.f_index(text, 'hover_menu": "."}, "', '"')
            self.filter_hashes = self.f_rfind_hashes(text)

        except ValueError:
            logging.info(' # Not more answers')
            return

        self.f_form_date()
        count, has_more = 0, True
        while has_more:
            count += 1
            response = self.session.post(url=self.server_call_url,
                                         data=self.data,
                                         headers=self.headers)
            try:
                data_json = json.loads(response.text)
            except ValueError:
                logging.debug(" # No more answers")
                return
            has_more = data_json['value']['has_more']
            self.filter_hashes += data_json['value']['hashes']
            self.f_form_date()
            logging.info(" + Get more answers - %s" % count)
            yield data_json['value']['html'].encode('utf-8')

    def more_questions(self, text):
        self.server_call_url = "http://www.quora.com/webnode2/server_call_POST"
        headers = dict(**self.headers)
        headers.update({'Accept': '*/*',
                        'Accept-Encoding': 'gzip, deflate',
                        'Content-Type': ('application/x-www-form-urlencoded'),
                        'Cookie': (r'm-b="5UKmqFM_NuYi4c1zDInSXA\075\075"; '
                                   'm-depd=b7f0ffabd45a2788; '
                                   'm-f=dZ8Ek3LTQdnbQ6v_'
                                   'JF0oHYdzJ4Kxm4eQ_z5U; '
                                   'm-mepd=f655728b6edd1e4b; '
                                   'm-s="YsS1U17IU8DJN0lOWOtPqA\075\075"; '
                                   'm-css_v=643cd6eed0f3f70318; '
                                   'm-tz=-180; m-screen_size=1680x1050; '
                                   '_ga=GA1.2.1598154934.1438650108; _gat=1'),
                        'Origin': 'http://www.quora.com',
                        'Referer': self.url})
        try:
            self.formkey = self.f_index(text, 'formkey: "')
            self.window_id = self.f_index(text, 'windowId: "')
            self.cid = self.f_rindex(text, '.QueryResults)("')
            self.filter_hashes = self.f_rfind_hashes(text)
            self.s_cmp = self.f_rindex(text, ('"Loading...", '
                                              '"serialized_component": "'))
            self.vcon_json = self.f_rindex(text, '"{}": "hmac:'.format(
                self.f_rindex(text, ('new(require("mobile_app2/view/paged_list"'
                                     ').PagedList)("'))))
        except ValueError:
            logging.debug('Substring not found. Reopen...')
            yield False
        self.f_form_date()
        count, perf = 0, 0
        while True:
            count += 1
            response = self.session.post(url=self.server_call_url,
                                         data=self.data,
                                         headers=self.headers)
            data_json = json.loads(response.text)
            if not data_json['value']['has_more']:
                if perf == 2:
                    return
                self.perf(headers)
                perf += 1
            else:
                perf = False
            self.filter_hashes += data_json['value']['hashes']
            self.f_form_date()
            logging.info(" + Get more questions - %s" % count)
            yield data_json['value']['html'].encode('utf-8')

    def perf(self, headers):
        perf_url = "http://www.quora.com/e2e/perf_POST"
        perf_time = time.time()*1000-2000
        p_id = self.base36encode(perf_time*1e3+math.floor(random.random()*1e3))
        data = json.dumps({"args": [{"vcon": '["hmac","%s"]' % self.vcon_json,
                                     "method": "mobile_get_next_page2",
                                     "startTime": perf_time,
                                     "id": p_id,
                                     "endTime": perf_time}],
                           "kwargs": {}})

        form_data = {'json': data,
                     'formkey': self.formkey,
                     'window_id': self.window_id,
                     'parent_cid': 'undefined',
                     'referring_controller': 'search',
                     'referring_action': 'index',
                     '__mobile_page': True}

        self.session.post(url=perf_url,
                          data=form_data,
                          headers=headers)
        logging.info(" ! Post next page - OK")


if __name__ == '__main__':
    #if len(sys.argv) != 3:
    #    print ('You must specify a search string and group.\n'
    #           'Example:\n\tpython parser.py "python" 36')
    #    sys.exit(0)
    #quora = Quora(sys.argv[1], sys.argv[2])
    #quora.parse()
    soo_lonely = SingleInstance()
    while True:
        with open('quora.txt', 'r') as f:
            first_line = f.readline().rstrip()
        keyword_l = first_line.split(":")[0]
        group_l = first_line.split(":")[1]
        quora = Quora(keyword_l, group_l)
        quora.parse()
        with open('quora.txt', 'r') as fin:
            data = fin.read().splitlines(True)
        with open('quora.txt', 'w') as fout:
            fout.writelines(data[1:])
